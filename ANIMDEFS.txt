//Idle
texture BFGNA0 NoTrim
texture BFGNB0 NoTrim
texture BFGNC0 NoTrim
texture BFGND0 NoTrim
texture BFGNE0 NoTrim

//Troonsformer 0 batt
texture BFL0A0 NoTrim
texture BFL0B0 NoTrim
texture BFL0C0 NoTrim
texture BFL0D0 NoTrim
texture BFL0E0 NoTrim

//Troonsformer 1 batt
texture BFL1A0 NoTrim
texture BFL1B0 NoTrim
texture BFL1C0 NoTrim
texture BFL1D0 NoTrim
texture BFL1E0 NoTrim

//Troonsformer 2 batt
texture BFL2A0 NoTrim
texture BFL2B0 NoTrim
texture BFL2C0 NoTrim
texture BFL2D0 NoTrim
texture BFL2E0 NoTrim

//Troonsformer 3 batt
texture BFL3A0 NoTrim
texture BFL3B0 NoTrim
texture BFL3C0 NoTrim
texture BFL3D0 NoTrim
texture BFL3E0 NoTrim

//Troonsformer 4 batt
texture BFL4A0 NoTrim
texture BFL4B0 NoTrim
texture BFL4C0 NoTrim
texture BFL4D0 NoTrim
texture BFL4E0 NoTrim

//Powerup sequence
texture BFGPA0 NoTrim
texture BFGPB0 NoTrim
texture BFGPC0 NoTrim
texture BFGPD0 NoTrim
texture BFGPE0 NoTrim
texture BFGPF0 NoTrim
texture BFGPG0 NoTrim
texture BFGPH0 NoTrim
texture BFGPI0 NoTrim
texture BFGPJ0 NoTrim
texture BFGPK0 NoTrim
texture BFGPL0 NoTrim
texture BFGPM0 NoTrim

//Charge to fire
texture BFGCA0 NoTrim
texture BFGCB0 NoTrim
texture BFGCC0 NoTrim
texture BFGCD0 NoTrim
texture BFGCE0 NoTrim
texture BFGCF0 NoTrim
texture BFGCG0 NoTrim
texture BFGCH0 NoTrim

//Fire
texture BFGFA0 NoTrim
texture BFGFB0 NoTrim
texture BFGFC0 NoTrim
### BFG
---
This is an addon for Deathstrider that adds the BFG. It has a chance to spawn on BFG spawns i.e. the same spawns as the Gungnir and Scorpion. The BFG requires 4 fully charged batteries to fire. Once fired the ball has a MASSIVE blast radius. Taking cover is advised. Protip: If you can see the ball, it can and will kill you.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Accensus, HalfBakedCake, cyb3r_c001 coding
- PillowBlaster, sprite assets
- [Project Brutality](https://projectbrutality.com/), some sound effects
- Icarus, sprites (Yes, I'm crediting myself. Yes, I have a massive ego.)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.